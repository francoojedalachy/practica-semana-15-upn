Algoritmo factorial
	definir num,fact,x Como Entero
	escribir"escriba un numero"
	leer num
	si num<0 Entonces
		escribir "el numero no se puede calcular"
	SiNo
		x<-1
		fact<-1
		Mientras x<=num Hacer
			fact<-fact*x
			x<-x+1
			
		Fin Mientras
		escribir"el factorial del numero ",num,"=",fact;
		
	FinSi
	
FinAlgoritmo
